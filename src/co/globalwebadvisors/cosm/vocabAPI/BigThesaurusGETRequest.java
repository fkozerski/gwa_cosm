package co.globalwebadvisors.cosm.vocabAPI;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.RequestTimeoutException;
import com.google.gwt.http.client.Response;

public class BigThesaurusGETRequest implements EntryPoint{

	private static final int STATUS_CODE_OK = 200;
	
	public static void doGet(String url) {
	    RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);

	    try {
	      Request response = builder.sendRequest(null, new RequestCallback() {
	        public void onError(Request request, Throwable exception) {
	          // Code omitted for clarity
	        }

	        public void onResponseReceived(Request request, Response response) {
	          // Code omitted for clarity
	        }
	      });
	    } catch (RequestException e) {
	      // Code omitted for clarity
	    }
	  }

	@Override
	public void onModuleLoad() {
		
		doGet("http://words.bighugelabs.com/api/2/14885adf7d5efeec85da840d774b0173/love/json");
	}
	

}


//@Override
//public void onResponseReceived(Request request, Response response) {
//	if (STATUS_CODE_OK == response.getStatusCode()) {
//	      // handle OK response from the server 
//	    } else {
//	      // handle non-OK response from the server
//	    }
//	
//}
//
//@Override
//public void onError(Request request, Throwable exception) {
//	if (exception instanceof RequestTimeoutException) {
//	      // handle a request timeout
//	    } else {
//	      // handle other request errors
//	    }
//	
//}