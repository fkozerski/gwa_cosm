package co.globalwebadvisors.cosm.server;

import co.globalwebadvisors.cosm.client.BigHugeThesaurusService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BigHugeThesaurusServiceImpl extends RemoteServiceServlet implements BigHugeThesaurusService {
	private final String USER_AGENT = "Mozilla/5.0";
	
	
	@Override
	public String getSynonyms(String cleanRootWord) {
		String url = "http://words.bighugelabs.com/api/2/14885adf7d5efeec85da840d774b0173/"+cleanRootWord+"/xml";
		 
		URL obj;
		StringBuffer response;
		try {
			obj = new URL(url);
			
			HttpURLConnection con;
		
			con = (HttpURLConnection) obj.openConnection();

						// optional default is GET
		con.setRequestMethod("GET");
 
		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);
 
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		
		if (200 == responseCode) {
				
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		response= new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		System.out.println(response.toString());
		return response.toString();
		}
		else{
			System.out.println("Unable to process response. Code: "+responseCode);
		}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "404";
		
	}
	
}
