package co.globalwebadvisors.cosm.server;

import java.util.ArrayList;
import java.util.HashMap;






import co.globalwebadvisors.cosm.client.MongoDBcrudService;
import co.globalwebadvisors.cosm.shared.PersistentEntity;
import co.globalwebadvisors.cosm.shared.SynonymModel;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

public class MongoDBcrudServiceImpl extends RemoteServiceServlet implements MongoDBcrudService{
	
	private MongoClient cosmMongo;
	DB cosm;
	DBCollection vocabulary;
	DBCollection keywords;
	DBCollection geolocations;
	
	public void refreshMongo(){
		try{
			cosmMongo = new MongoClient(new ServerAddress("54.88.8.27", 27017));
			
			
			cosm = cosmMongo.getDB("cosm");
			vocabulary = cosm.getCollection("vocabulary");
//			keywords = cosm.getCollection("keywords");
//			geolocations = cosm.getCollection("geolocations");
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception "+e.getLocalizedMessage() + "\n " + e.getMessage());
		}
		
	}
	
	public void update(PersistentEntity[] data){
		for(PersistentEntity aData : data){
			if(aData.getClass() == SynonymModel.class){
				//TODO create classes & handle I/O here
			}
		}
	}
	
	@Override
	public void update(String[] data) {
		if(data == null || data.length == 0){
			return;
		}
		if (cosmMongo == null){
			refreshMongo();
		}
		DBObject[] saveObjects = createVocabObjects(data);
		if(saveObjects == null || saveObjects.length < 1){
			return;
		}
		Boolean[] mongoContains = queryMongoArray(parseAttribute("word",data)).values().toArray(new Boolean[saveObjects.length]);
		
//		try{
			for(int i = 0; i < saveObjects.length; i++){
				DBObject aVocab = saveObjects[i];
				if(aVocab == null){
					continue;
				}
				if(!mongoContains[i]){
					vocabulary.save(aVocab,new WriteConcern(0));	
				}
			}
//		}catch(Exception E){
//			System.out.print(E.getMessage());
//			E.printStackTrace();
//		
//		}
	}
	
	//TODO clean this up to remove unneccesary processing
	private DBObject[] createVocabObjects(String[] data) {
		DBObject[] out = new DBObject[data.length];
		
		for(int i = 0; i < data.length; i++){
			String[] vocabString = data[i].split(" : ");
			DBObject vocabData =  new BasicDBObject("rootWord", vocabString[3]).append("word", vocabString[0]).append("partOfSpeech", vocabString[1]).append("relationship", vocabString[2]);
			out[i] = vocabData;
		}
		return out;
	}
	
	private String[] parseAttribute(String attr, String[] data){
		String[] out = new String[data.length];
		int index = -1;
		if(attr.compareTo("rootWord") == 0){
			index = 3;
		}
		else if(attr.compareTo("word") == 0){
			index = 0;
		}
		else if(attr.compareTo("partOfSpeech") == 0){
			index = 1;
		}
		else if(attr.compareTo("relationship") == 0){
			index = 2;
		}
		else{
			return out;
		}
		for(int i =0; i < data.length; i++){
			out[i] = data[i].split(" : ")[index];
		}
		
		return out;
	}

	@Override
	public String readSynonyms(String rootWord) {
		if (cosmMongo == null){
			refreshMongo();
		}
		String out = "";
		
		return out;
	}

	@Override
	public String[] queryMongo(String[] labelValueQuery) {
		ArrayList<String> queryResult = new ArrayList<String>(); 
		if (cosmMongo == null){
			refreshMongo();
		}
		DBCursor cursor = vocabulary.find();
		for(int i = 0; i < labelValueQuery.length / 2; i++){
			
		}
		BasicDBObject query = new BasicDBObject(labelValueQuery[0], labelValueQuery[1]);

		cursor = vocabulary.find(query);
		
		try {
		   while(cursor.hasNext()) {
			   queryResult.add(cursor.next().toString());
//		       System.out.println(cursor.next());
		   }
		} finally {
		   cursor.close();
		}
		
		return queryResult.toArray(new String[queryResult.size()]);
	}
	
	/**
	 * 
	 * @param queryWords do these words exist in mongo?
	 * @return
	 */
	private HashMap<String,Boolean> queryMongoArray(String[] queryWords){
		String[] queryResults = new String[queryWords.length], querySet;
		HashMap<String,Boolean> out = new HashMap<String, Boolean>();
		
		for(int i = 0; i < queryWords.length; i++){
			querySet = new String[]{"word", queryWords[i]};
			out.put(queryWords[i], (queryMongo(querySet).length > 0) ? new Boolean(true): new Boolean(false));
		}
		
		return out;
	}

	@Override
	public Void addSynonym(String rootWord, String synonym) {
		String[] synonymArray = new String[]{synonym};
		HashMap<String,Boolean> mongoData = queryMongoArray(synonymArray);
		if(mongoData.get(synonymArray[0])){//Data exists in Mongo. we modify existing data
			queryMongo(new String[]{"word", synonymArray[0]});
		}
		else{//Data does not exist in mongo. we must create new data.
			
		}
		return null;
	}
	
	
}
