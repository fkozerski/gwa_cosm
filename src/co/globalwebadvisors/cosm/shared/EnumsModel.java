package co.globalwebadvisors.cosm.shared;

import co.globalwebadvisors.cosm.shared.app.Composed;

public class EnumsModel {

	public enum GenderEnum {Male, Female, Trans};
	public enum ComplexityEnum {Informal, Common, Formal, College, PhD, Jargon};
	public enum RegionEnum {SouthEast, Midwest, PacificIslands, NorthEast, WestCoast, Rockies, Appalachias};
	public enum IndustryEnum {AutoCollision};
	public enum GeolocationEnum {Atlanta};
	public enum ClientEnum {};
	public enum CRUDop {CREATE,READ,UPDATE,DELETE};	
	

	public enum Relationship {ANT, SYN, REL, UNREL;
		
		public Relationship get(Relationship rel, Composed A, Composed B){
			if(rel == Relationship.ANT){
				
			}
			return null;
		}
	
	}
	public enum PartOfSpeech {NOUN, PRON, ADJ, ADV, VERB, PREP, CONJ, INTR;
	}

}
