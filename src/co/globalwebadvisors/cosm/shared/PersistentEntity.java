package co.globalwebadvisors.cosm.shared;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import co.globalwebadvisors.cosm.shared.EnumsModel.ComplexityEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.GenderEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.RegionEnum;
import co.globalwebadvisors.cosm.shared.app.Composed;




public abstract class PersistentEntity extends Composed implements Serializable{
  private static final long serialVersionUID = 1L;
	
  private String userName;
    
  private int numberTimesUsed;
  private int numberTimesQueried;
  private Date timeLastUsed;
  private Date timeLastQueried;
  
  public String getUserName() {
	return userName;
  }
  public void setUserName(String userName) {
	this.userName = userName;
  }
//
  //TODO use a real password handler
//  public String getPassword() {
// 	return password;
//  }
//  public void setPassword(String password) {
//	this.password = password;
//  }
  
  /**Override in child classes to handle the differences between permuting keywords, geolocations, industries, & variables
   * 
   * @return
   */
  public abstract ArrayList<PersistentEntity> getPermutations();
  
  
}