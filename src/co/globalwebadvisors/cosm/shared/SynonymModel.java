package co.globalwebadvisors.cosm.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import co.globalwebadvisors.cosm.shared.EnumsModel.PartOfSpeech;
import co.globalwebadvisors.cosm.shared.Globals.*;
import co.globalwebadvisors.cosm.shared.EnumsModel.ComplexityEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.GenderEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.RegionEnum;
import co.globalwebadvisors.cosm.shared.app.Composable;
import co.globalwebadvisors.cosm.shared.app.Composed;

import co.globalwebadvisors.cosm.shared.Globals.WordTuple;


public class SynonymModel extends PersistentEntity {
	
	private static ArrayList<SynonymModel> cachedModels = new ArrayList<SynonymModel>();
	
	
	//Instance
	private ArrayList<String> rootWords;
	private HashMap<WordTuple, PartOfSpeech> relationship = new HashMap<WordTuple, PartOfSpeech>();

	private PartOfSpeech partOfSpeech;
	private String word;
	
	private HashMap<Tuple, PartOfSpeech> relationshipMap;
	
	private String verbTense;
	private String prefix;
	private String suffix;
	private String punctuation;
	
	private ComplexityEnum complexity;
	private GenderEnum gender; 
	private RegionEnum region;

	private Boolean plurality;
	private Boolean phrase;
	private Boolean clause;
	
	//stores the most commonly seen contexts of this entity, along with the contextual entities and the frequency
	//TODO count frequency, one-to-many & many-to-one entity maps
	private HashMap<String[], Integer> colocationCounts;
	
	private int numberTimesUsed;
	private int numberTimesQueried;
	
	private Date timeLastUsed;
	private Date timeLastQueried;
	
	public SynonymModel(){
		this.parts = new ArrayList<Composed>();
		this.parents = new ArrayList<Composed >();
		
		
		
		relationshipMap = new  HashMap<Tuple, PartOfSpeech>();
		colocationCounts = new HashMap<String[], Integer>();
		return;		
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6206899704646463221L;
	
	public static SynonymModel createSynonym(String word){
		SynonymModel out = null;
		for(SynonymModel cachedWord : cachedModels){
			
		}
		return out;
	}
	
	public static void writeToMongo(){
		
	}
	
	public static void append(ArrayList<SynonymModel> result) {
		for(SynonymModel result_synonym : result){
			if(cachedModels.size() < 1){
				cachedModels.addAll(result);
			}
			else{
				for(SynonymModel cached_synonym : cachedModels){
					if(cached_synonym != result_synonym){
						cachedModels.add(result_synonym);
					}
				}
			}
		}
	}

	public ArrayList<SynonymModel> getCachedModels() {
		return cachedModels;
	}

	public void setCachedModels(ArrayList<SynonymModel> cachedModels) {
		this.cachedModels = cachedModels;
	}
	
	//-----------------------INSTANCE-----------------------------
	public ArrayList<String> getRootWords() {
		return rootWords;
	}

	public void setRootWords(ArrayList<String> rootWords) {
		this.rootWords = rootWords;
	}
	
	public void setWord(String word) {
		this.word = word;
	}

	public PartOfSpeech getPartOfSpeech() {
		return partOfSpeech;
	}

	public void setPartOfSpeech(PartOfSpeech partOfSpeech) {
		this.partOfSpeech = partOfSpeech;
	}

	public HashMap<WordTuple, PartOfSpeech> getRelationship() {
		return relationship;
	}

	public void setRelationship(HashMap<WordTuple, PartOfSpeech> relationship) {
		this.relationship = relationship;
	}

	public int getNumberTimesUsed() {
		return numberTimesUsed;
	}

	public void setNumberTimesUsed(int numberTimesUsed) {
		this.numberTimesUsed = numberTimesUsed;
	}

	public int getNumberTimesQueried() {
		return numberTimesQueried;
	}

	public void setNumberTimesQueried(int numberTimesQueried) {
		this.numberTimesQueried = numberTimesQueried;
	}

	public Date getTimeLastUsed() {
		return timeLastUsed;
	}

	public void setTimeLastUsed(Date timeLastUsed) {
		this.timeLastUsed = timeLastUsed;
	}

	public Date getTimeLastQueried() {
		return timeLastQueried;
	}

	public void setTimeLastQueried(Date timeLastQueried) {
		this.timeLastQueried = timeLastQueried;
	}

	@Override
	public ArrayList<PersistentEntity> getPermutations() {
		// TODO SYNONYM PERM - for each synonym  
		return null;
	}
	
	
	
	public String getWord(){
		return word;
	}
	
	
	public SynonymModel[] getAllWords(){
		ArrayList<Composable> out = new ArrayList<Composable>();
		for(Composed s : this.parts){
			out.add(s);
		}
		for(Composed s : this.parents){
			out.add(s);
		}
		
		return out.toArray(new SynonymModel[out.size()]);
	}
	
	

	private void updateRelationshipMap(SynonymModel three) {
		
	}

	
}
