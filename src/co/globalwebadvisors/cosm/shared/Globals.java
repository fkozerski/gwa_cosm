package co.globalwebadvisors.cosm.shared;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.gwt.core.client.GWT;

import co.globalwebadvisors.cosm.client.BigHugeThesaurusService;
import co.globalwebadvisors.cosm.client.BigHugeThesaurusServiceAsync;
import co.globalwebadvisors.cosm.client.MongoDBcrudService;
import co.globalwebadvisors.cosm.client.MongoDBcrudServiceAsync;
import co.globalwebadvisors.cosm.shared.app.Composed;

public class Globals {
	
	public static Globals global = new Globals();
	
	public BigHugeThesaurusServiceAsync bHThesaurusSvc = GWT.create(BigHugeThesaurusService.class);
	public MongoDBcrudServiceAsync mongoCRUDSvc = GWT.create(MongoDBcrudService.class);
	
	private Globals(){};
	
	public static Globals getGlobals(){
		if(global == null){
			global = new Globals();
		}
		return global;
	}
	
	public class WordTuple implements Comparable{
		String rootWord, relatedWord;
		
		public WordTuple(String rootWord, String relatedWord){
			this.relatedWord = relatedWord;
			this.rootWord = rootWord; 
		}

		@Override
		public int compareTo(Object o) {
			String[] tuple = (String[]) o;
			int out = -1;
			if(tuple[0].compareTo(rootWord) == 0 && tuple[1].compareTo(relatedWord) == 0){
				out = 0;
			}
			return out;
		}
		
		
		
		
	}
	
}
