package co.globalwebadvisors.cosm.shared.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import co.globalwebadvisors.cosm.shared.EnumsModel.Relationship;
import co.globalwebadvisors.cosm.shared.Tuple;


public class Composed implements Composable, Comparable{
	private static long baseID = 0l; 
	
	protected long cID;
	
	protected ArrayList<Composed> parts;
	protected ArrayList<Composed> parents;
	
	protected HashMap<Composed, Integer> useCount;
	protected HashMap<Tuple, Relationship> relationship;  
	
	public Composed(){
		cID = baseID;
		baseID++;
	}
	
//=*=*=*=*=*=*=*=*=*=*=*=HERE BE RECURSIVE DRAGONS=*=*=*=*=*=*=*=*=*=*=*=
	public ArrayList<Composed> get(Composed get){
		ArrayList<Composed> out = new ArrayList<Composed>();

		if(get.getClass() == this.getClass()){
			out.add(this);
			return out;
		}
		
		for(Composed part : this.getComposedOf()){
			out.addAll(part.get(get));
		}
		
		return out;
	}
		
	public Composed[] getComposedOf(){
		return parts.toArray(new Composed[parts.size()]);
	}
	public Composed[] getParent(){
		return parents.toArray(new Composed[parents.size()]);
	}
	
	public void addPartOf(Composed[] add){
		if(add == null){
			return;
		}
		for(Composed three : add){
			if(!parts.contains(three)){
				parts.add(three);
			}
		}
		
	}
	
	public void removePartOf(Composed[] remove){
		if(remove == null){
			return;
		}
		for(Composed three : remove){
			if(parts.contains(three)){
				parts.remove(three);
			}
		}
		
	}
	public void addParent(Composed[] newParent){
		if(newParent == null){
			return;
		}
		for(Composed three : newParent){
			if(!parents.contains(three)){
				parents.add(three);
			}
		}
	}
	
	public void removeParent(Composed[] remove){
		if(remove == null){
			return;
		}
		for(Composed three : remove){
			if(parents.contains(three)){
				parents.remove(three);
			}
		}
	}

	@Override
	public int compareTo(Object arg0) {
		Composed comp = (Composed) arg0;
		int out = -2;
		if(cID == comp.cID){
			out = 0;
		}
		else if(cID < comp.cID){
			out = -1;
		}
		else if(cID > comp.cID){
			out = 1;
		}
		return out;
	}
	
}
