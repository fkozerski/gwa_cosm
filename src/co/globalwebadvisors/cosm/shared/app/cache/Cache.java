package co.globalwebadvisors.cosm.shared.app.cache;

import co.globalwebadvisors.cosm.shared.Globals;

//stores data locally for faster access

public class Cache<T> {
	
	public void read(T type){
		Globals.getGlobals().mongoCRUDSvc.read(type);
	}

	public void create(T type){
		Globals.getGlobals().mongoCRUDSvc.create(type);
	}
	
	public void update(T type){
		Globals.getGlobals().mongoCRUDSvc.update(type);
	}
	
	public void delete(T type){
		Globals.getGlobals().mongoCRUDSvc.delete(type);
	}
	
	
	
}
