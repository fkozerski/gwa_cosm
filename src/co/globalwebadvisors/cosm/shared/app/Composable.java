package co.globalwebadvisors.cosm.shared.app;

import java.util.ArrayList;

public interface Composable {

	public ArrayList<Composed> get(Composed get);
	
	public Composed[] getComposedOf();
	
	public void addPartOf(Composed[] add);
	
	public void removePartOf(Composed[] remove);
	
	public void addParent(Composed[] newParent);
	
	public void removeParent(Composed[] remove);
	
	
}
