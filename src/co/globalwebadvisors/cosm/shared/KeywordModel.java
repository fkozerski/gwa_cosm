package co.globalwebadvisors.cosm.shared;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import co.globalwebadvisors.cosm.shared.EnumsModel.ClientEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.ComplexityEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.GenderEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.GeolocationEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.IndustryEnum;
import co.globalwebadvisors.cosm.shared.EnumsModel.RegionEnum;
import co.globalwebadvisors.cosm.shared.app.Composed;

public class KeywordModel extends PersistentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5835488311778570534L;
	
		
	  private ArrayList<KeywordModel> keywords;
	  private ArrayList<String> words;
	  private String partofSpeech;
	  private String prefix;
	  private String suffix;
	  private String punctuation;
	  
	  
	  //stores the most commonly seen contexts of this entity, along with the contextual entities and the frequency
	  //TODO count frequency, one-to-many & many-to-one entity maps
	  private ComplexityEnum complexity;
	  private GenderEnum gender; 
	  private RegionEnum region;
	  
	  private Boolean plurality;
	  private Boolean phrase;
	  private Boolean clause;
	
	
	  private IndustryEnum industry;
	  private GeolocationEnum geoloc;
	  private ClientEnum client;

	@Override
	public ArrayList<PersistentEntity> getPermutations() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Composed[] getParent() {
		return new Composed[0];
	}
	@Override
	public void addPartOf(Composed[] add) {
		List<Composed> one = Arrays.asList(add);
		KeywordModel[] two = one.toArray(new KeywordModel[one.size()]);
		for(KeywordModel three : two){
			keywords.add(three);
		}
		
	}
	@Override
	public void removePartOf(Composed[] remove) {
		List<Composed> one = Arrays.asList(remove);
		KeywordModel[] two = one.toArray(new KeywordModel[one.size()]);
		for(KeywordModel three : two){
			keywords.remove(three);
		}
	}
	@Override
	public void addParent(Composed[] newParent) {
		return;
	}
	@Override
	public void removeParent(Composed[] remove) {
		return;
	}
	
	
}
