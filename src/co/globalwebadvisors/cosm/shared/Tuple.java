package co.globalwebadvisors.cosm.shared;

import java.util.ArrayList;
import java.util.Arrays;

import co.globalwebadvisors.cosm.shared.app.Composed;

public class Tuple implements Comparable{
		
		public Composed one;
		public Composed two;
		
		public  Tuple(Composed objOne, Composed objTwo){
			one = objOne;
			two = objTwo;
		}
		
		@SuppressWarnings("unchecked")
		public Tuple[] createTupleTable(){
			ArrayList<Tuple> out = new ArrayList<Tuple>();
			
			ArrayList<Composed> inOne = new ArrayList(Arrays.asList(one.getParent()));
			ArrayList<Composed> inTwo = new ArrayList(Arrays.asList(two.getParent()));
			
			for(Composed a : inOne){				
				for(Composed b : inTwo){
					out.add(new Tuple(a,b));
				}
			}
			
			return out.toArray(new Tuple[out.size()]);
		}
		
		@Override
		public int compareTo(Object arg0) {
			int out = -1;
			Tuple comp = (Tuple) arg0;
			if(one.compareTo(comp.one) == 0 && two.compareTo(comp.two) == 0){
				out = 0;
			}

			return out;
		}
		
}
	
