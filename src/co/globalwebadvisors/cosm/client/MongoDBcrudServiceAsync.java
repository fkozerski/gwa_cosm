package co.globalwebadvisors.cosm.client;
import java.util.ArrayList;

import co.globalwebadvisors.cosm.shared.SynonymModel;


//
import com.google.gwt.user.client.rpc.AsyncCallback;
//
public interface MongoDBcrudServiceAsync{
//
//
	void update(String[] data, AsyncCallback<Void> callback);
//
	void readSynonyms(String rootWord, AsyncCallback<String> callback);
    void queryMongo(String[] labelValueQuery, AsyncCallback<String[]> callback);
	void addSynonym(String rootWord, String synonym, AsyncCallback<Void> callback);
}