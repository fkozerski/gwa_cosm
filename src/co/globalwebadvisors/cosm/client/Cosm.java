package co.globalwebadvisors.cosm.client;

import java.util.ArrayList;
import java.util.HashMap;

import java_cup.shift_action;
import co.globalwebadvisors.cosm.shared.FieldVerifier;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasMouseDownHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.core.java.util.Arrays;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Cosm implements DialogBoxOpener, EntryPoint  {
	//DialogBoxOpener
	private static final String TEXT_CORPUS = "In linguistics, a corpus (plural corpora) or text corpus is a large and structured set of texts (nowadays usually electronically stored and processed). They are used to do statistical analysis and hypothesis testing, checking occurrences or validating linguistic rules within a specific language territoryA corpus may contain texts in a single language (monolingual corpus) or text data in multiple languages (multilingual corpus). Multilingual corpora that have been specially formatted for side-by-side comparison are called aligned parallel corpora." +
									"In order to make the corpora more useful for doing linguistic research, they are often subjected to a process known as annotation. An example of annotating a corpus is part-of-speech tagging, or POS-tagging, in which information about each word's part of speech (verb, noun, adjective, etc.) is added to the corpus in the form of tags. Another example is indicating the lemma (base) form of each word. When the language of the corpus is not a working language of the researchers who use it, interlinear glossing is used to make the annotation bilingual.";
	private ArrayList<Button> buttonCorpus = new ArrayList<Button>();
	private HashMap<Button,String> buttonText = new HashMap<Button, String>();
	private boolean controlWasDown = false;
	private ArrayList<String> controlClicked = new ArrayList<String>();
	private ArrayList<SynonymDialogBox> dialogBoxes = new ArrayList<SynonymDialogBox>(); 
	
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
				
		ArrayList<String> arrayCorpus = new ArrayList<String>(java.util.Arrays.asList(TEXT_CORPUS.split(" ")));
		for(String aWord : arrayCorpus){
			buttonCorpus.add(new Button(aWord));
		}
		
		final Button sendButton = new Button("Submit");
//		final TextBox nameField = new TextBox();
//		nameField.setText("GWT User");
		final Label errorLabel = new Label();

		// We can add style names to widgets
		for(Button aButton : buttonCorpus){
			aButton.addStyleName(aButton.getText() + " #:" + buttonCorpus.indexOf(aButton));
			aButton.addStyleName("wordButton");
		}
		sendButton.addStyleName("sendButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
//		RootPanel.get("nameFieldContainer").add(nameField);
		
		//TODO break this process of creating buttons/objects out into a factory
		for(Button aButton : buttonCorpus){
			RootPanel.get("corpusFieldContainer").add(aButton);
		}
		
		RootPanel.get("sendButtonContainer").add(sendButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);

		// Focus the cursor on the name field when the app loads
//		nameField.setFocus(true);
//		nameField.selectAll();

		// Create the popup dialog box
//		final SynonymDialogBox dialogBox = new SynonymDialogBox();
//		dialogBox.setText("Select Synonyms");

	
		class sendHandler implements  ClickHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendButton(event);
			}
			
			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendButton(ClickEvent event) {
				
//				// Then, we send the input to the server.
//				sendButton.setEnabled(false);
//				textToServerLabel.setText("blank");
//				serverResponseLabel.setText("");
//				String textToServer = textToServerLabel.getText();
//				greetingService.greetServer(textToServer, new AsyncCallback<String>() {
//							public void onFailure(Throwable caught) {
//								// Show the RPC error message to the user
//								dialogBox
//										.setText("Remote Procedure Call - Failure");
//								serverResponseLabel
//										.addStyleName("serverResponseLabelError");
//								serverResponseLabel.setHTML(SERVER_ERROR);
//								dialogBox.center();
//								sendButton.setFocus(true);
//							}
//
//							public void onSuccess(String result) {
//								dialogBox.setText("Remote Procedure Call");
//								serverResponseLabel
//										.removeStyleName("serverResponseLabelError");
//								serverResponseLabel.setHTML(result);
//								dialogBox.center();
//								sendButton.setFocus(true);
//							}
//						});
			}

		}
		
		// Create a handler for the sendButton and nameField
		class MyHandler implements MouseDownHandler {
			SynonymDialogBox dialogBox;
			
//			/**ClickHandler
//			 * Fired when the user clicks on a word Button.
//			 */
//			@Override
//			public void onClick(ClickEvent event) {
//				event.getNativeButton();
//				
//			}
			
			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void getThesaurus(MouseDownEvent event, DialogBoxOpener ref) {
				dialogBox= getSynonymDialogBox();
				
				dialogBox.setAnimationEnabled(true);
				
				Button clickedButton = (Button) event.getSource();//event-> source root panel id -> button object, if no other way
				
				
				sendButton.setEnabled(true);
				String textToServer = clickedButton.getHTML();
				if(textToServer.contains("<")){
					String[] textArray = textToServer.split("<.*?>");
				
					dialogBox.setText(textArray[1]);
				}
				else{
					dialogBox.setText(textToServer);
				}
				dialogBox.myShowDialog(ref);
				
			    }
			
			private void getThesaurus(String word, DialogBoxOpener ref){
				dialogBox.setText(word);
				dialogBox.myShowDialog(ref);
			}

			@Override
			public void onMouseDown(MouseDownEvent event) {
				SynonymDialogBox box = new SynonymDialogBox();
				
				
				switch(event.getNativeButton()){
				case com.google.gwt.dom.client.NativeEvent.BUTTON_LEFT:
					article_mouse_BUTTON_LEFT(event);
					break;
				case com.google.gwt.dom.client.NativeEvent.BUTTON_RIGHT:
					article_mouse_BUTTON_RIGHT(event);
					break;
				case com.google.gwt.dom.client.NativeEvent.BUTTON_MIDDLE:
					article_mouse_BUTTON_MIDDLE(event);
					break;
				}
				
			}


			private void article_mouse_BUTTON_MIDDLE(MouseDownEvent event) {
				// TODO Auto-generated method stub
				
			}


			private void article_mouse_BUTTON_RIGHT(MouseDownEvent event) {
					Button orig_button = (Button) event.getSource();
					if(buttonText.get(orig_button) != null){
						dialogBoxButtonText(orig_button.getHTML(), new String[]{buttonText.get(orig_button)});
					}
			}

			private String parseWordButtonHTML(String listBoxString){
				String out = "";
				String[] listBoxArray = listBoxString.split(" : "); 
				out = listBoxArray[0];
				return out;
			}
			
			private void article_mouse_BUTTON_LEFT(MouseDownEvent event) {
				//THIS WILL PUT ALL WORDS IN 1 WINDOW needs more UI work
//				if(event.isControlKeyDown()){
//					controlWasDown = true;
//					
//					String textToServer = ((Button)event.getSource()).getHTML();
//					if(textToServer.contains("<")){
//						String[] textArray = textToServer.split("<.*?>");
//						controlClicked.add(textArray[1]);
//					}
//					else{
//						controlClicked.add(textToServer);
//					}
//					
//					
//				}
//				else if(controlWasDown){
//					controlWasDown = false;
//					for(String word : controlClicked){
//						getThesaurus(word, Cosm.this);
//					}
//					controlClicked.clear();
//				}
//				else{
//					controlWasDown = false;
//					getThesaurus(event,Cosm.this);
//				}
				getThesaurus(event,Cosm.this);
			}


		
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		sendHandler sendHandler = new sendHandler();
//		sendButton.addClickHandler(sendHandler);
		
		for(Button aButton : buttonCorpus){
			aButton.addMouseDownHandler(handler);
		}
		
//		nameField.addKeyUpHandler(handler);
	}
	
	private SynonymDialogBox getSynonymDialogBox(){
		if(dialogBoxes == null){
			dialogBoxes = new ArrayList<SynonymDialogBox>();
		}
		if(dialogBoxes.size() < 1){
			dialogBoxes.add(new SynonymDialogBox());
			return dialogBoxes.get(0);
		}
		for(SynonymDialogBox box : dialogBoxes){
			if(!box.isInUse()){
				return box;
			}
		}
		dialogBoxes.add(new SynonymDialogBox());
		return dialogBoxes.get(dialogBoxes.size() -1);
		
	}
	
	/**
	 * 
	 */
	@Override
	public void dialogBoxButtonText(String original, String[] updatedText) {
		String uiHTML = formatUIButtonText(updatedText);	
		
		
		for(Button button : buttonCorpus){//for all buttons
			if(button.getHTML().compareTo(original) == 0){//if button matches original string
				if(updatedText.length > 0){
					buttonText.put(button, button.getHTML());//for swapping out later
					
					
//					for(String word : updatedText){//add text to button
//						uiText += word + " \n ";
//					}
//					button.setHeight(new Integer(1 * updatedText.length).toString());
					button.setHTML(uiHTML);
				}//updatedText > 0
			}//button matches
		}//button loop
	}
	
	private String formatUIButtonText(String[] wordList){
		String out = new String();
		if(wordList[0].contains("<")){
			return wordList[0];
		}
		
		out = "<ol> <li id='0'>" + wordList[0] + "</li>";
		
		for(int i = 1; i <  wordList.length; i++){
			out = out.concat("<li id='"+i+"' >" + wordList[i].toString() + "</li>");
		}
		
		out = out.concat("</ol>");
		return out;
	}
	
}
