package co.globalwebadvisors.cosm.client;

import java.util.ArrayList;

public interface DialogBoxOpener {
	void dialogBoxButtonText(String original, String[] updatedText);
}
