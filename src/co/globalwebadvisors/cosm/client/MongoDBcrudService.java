package co.globalwebadvisors.cosm.client;

import java.util.ArrayList;

import co.globalwebadvisors.cosm.shared.SynonymModel;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

//import com.google.gwt.user.client.rpc.RemoteService;
//import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
//
@RemoteServiceRelativePath("MongoDBcrud")
public interface MongoDBcrudService extends RemoteService{

	/**
	 * 
	 * @param data array of String data to update Mongo with
	 */
	void update(String[] data);
	
	/**
	 * 
	 * @param rootWord lookup synonyms
	 * @return
	 */
	String readSynonyms(String rootWord);


	String[] queryMongo(String[] labelValueQuery);

	Void addSynonym(String rootWord, String synonym);
}
