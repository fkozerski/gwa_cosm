package co.globalwebadvisors.cosm.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface BigHugeThesaurusServiceAsync {

	void getSynonyms(String cleanRootWord, AsyncCallback<String> callback);

}
