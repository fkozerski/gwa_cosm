/**
 * 
 */
package co.globalwebadvisors.cosm.client;

import java.util.ArrayList;
import java.util.Arrays;

import co.globalwebadvisors.cosm.shared.Globals;
import co.globalwebadvisors.cosm.shared.SynonymModel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyEvent;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.regexp.shared.SplitResult;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.Text;
import com.google.gwt.xml.client.XMLParser;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author nix
 *
 */
public class SynonymDialogBox extends DialogBox implements HasText {

	private static SynonymDialogBoxUiBinder uiBinder = GWT
			.create(SynonymDialogBoxUiBinder.class);
	
	private static final boolean MONGO_DEBUG = true;
	
	private String dirtyRootWord;
	private DialogBoxOpener opener = null;
	private ArrayList<Integer> selectedIndecies;
	private ArrayList<String> selectedWords;
	private String prefix = "",	suffix = "";
	
	//TODO display error message
	private Label errorMsgLabel = new Label();
	
	
	private boolean instanceInUse = false; 
	
//	((ServiceDefTarget)bHThesaurusSvc).setServiceEntryPoint("/cosm/services/bigHugethesaurus");
	interface SynonymDialogBoxUiBinder extends
			UiBinder<Widget, SynonymDialogBox> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SynonymDialogBox(){
		this.setModal(false);
		setWidget(uiBinder.createAndBindUi(this));
		
		selectedWords = new ArrayList<String>();
		selectedIndecies = new ArrayList<Integer>();
		
		synonymScrollPanel.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
					boolean doAdd = true;
					
					if(synonymScrollPanel.isMultipleSelect()){
						ArrayList<String> wordsToAdd = getMultiSelect();
						
						selectedWords = wordsToAdd;
//						for(String wordToAdd : wordsToAdd){
//							doAdd = true;
//							for(String selected : selectedWords){
//								if(selected.compareTo(wordToAdd) == 0){
//									doAdd=false;
//								}
//							}
//							if(doAdd){
//								selectedWords.add(wordToAdd);
//							}
//						}
						
						
					}
					else{
						String wordToAdd = parseWordFromListBox(synonymScrollPanel.getValue(synonymScrollPanel.getSelectedIndex()));
					
						for(String selected : selectedWords){
							if(selected.compareTo(wordToAdd) == 0){
								doAdd = false;
							}
						}
						if(doAdd){
							selectedWords.add(wordToAdd);
						}
					}
					for(Integer index : selectedIndecies){
						synonymScrollPanel.setItemSelected(index, true);
					}
			}
		});
		
//		synonymScrollPanel = new ListBox();
//		synonymScrollPanel.setStylePrimaryName("synonymScrollPanel");
//		scrollContainer = new VerticalPanel();
//		scrollContainer.setStylePrimaryName("scrollContainer");
		
//		scrollContainer.add(synonymScrollPanel);
		
	}
	
	
//	public showDialogBox(final ){
//		
//	}
	
	protected ArrayList<String> getMultiSelect() {
		selectedIndecies = new ArrayList<Integer>();
		ArrayList<String> out = new ArrayList<String>();
		
		RegExp alpha = RegExp.compile("\\w");
		SplitResult nonAlphaStrip = alpha.split(dirtyRootWord);
		prefix = nonAlphaStrip.get(0).toString();
		suffix = nonAlphaStrip.get(nonAlphaStrip.length() -1 ).toString();
		
		for(int i = 0; i < synonymScrollPanel.getItemCount(); i++){
			if(synonymScrollPanel.isItemSelected(i)){
				selectedIndecies.add(i);
				out.add(prefix+synonymScrollPanel.getItemText(i).split(" : ")[0]+suffix);//CAN INCLUDE IN LISTBOX ALL PARTS OF WORD DEF HERE
			}
		}
		return out;
	}


	@UiField Button synonymSubmitButton;
	@UiField TextBox rootWordTextField;
	@UiField TextBox inputSynonym;
	@UiField Button synonymCloseButton;
	@UiField Label synonymBoxLabel;
	@UiField ListBox synonymScrollPanel;
	@UiField VerticalPanel rootContainer;
	@UiField VerticalPanel scrollContainer;
	//auto select added syn for root word
	public void lookupRootWord(String dirtyRootWord) {
		this.dirtyRootWord = dirtyRootWord;
		String cleanRootWord = textFormat(dirtyRootWord);
		
		
		getSynonyms(cleanRootWord);
		
		rootWordTextField.setText(dirtyRootWord);		
	}
	

	
	private String parseWordFromListBox(String listBoxString){
		String out = "";
		String[] listBoxArray = listBoxString.split(" : "); 
		out = listBoxArray[0];
		return out;
	}
	
	@UiHandler("synonymSubmitButton")
	void onSubmitClick(ClickEvent submit) {
		//update mongo
		//TODO update mongo data
		
		//update button text
		String[] buttonText = new String[selectedWords.size() + 1];
		buttonText[0] = rootWordTextField.getText();
		for(int i = 1; i < buttonText.length; i++){
			buttonText[i] = selectedWords.get(i-1);
		}
		
		opener.dialogBoxButtonText(dirtyRootWord, buttonText);
		
		selectedWords.clear();
		selectedIndecies.clear();
		prefix = "";
		suffix = "";
		instanceInUse = false;
		this.hide();
	}
	
	@UiHandler("inputSynonym")
	void onKeyUpInput(KeyUpEvent event){
		if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER){
			getSynonyms(inputSynonym.getText());
			
//			if (mongoCRUDSvc == null) {
//				mongoCRUDSvc = GWT.create(MongoDBcrudService.class);
//			}
		
			// Set up the callback object.
			AsyncCallback<Void> callback = new AsyncCallback<Void>() {

				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Void result) {
					
				}
			};
//			 Make the call to the stock price service.
			Globals.getGlobals().mongoCRUDSvc.addSynonym(textFormat(dirtyRootWord), inputSynonym.getText(), callback);
		};
			
	}
	
	@UiHandler("synonymCloseButton")
	void onCloseClick(ClickEvent close){
		synonymScrollPanel.clear();
		selectedWords.clear();
		selectedIndecies.clear();
//		for(int i = 0; i < synonymScrollPanel.getItemCount(); i++){
//			synonymScrollPanel.removeItem(i);
//		}
		prefix = "";
		suffix = "";
		instanceInUse = false;
		this.hide();
	}
	//TODO current does not account for complete changes to the word, only edits
	@UiHandler("rootWordTextField")
	void onKeyUpRoot(KeyUpEvent event) {
		if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
			updateDirtyRootWord(event);
		}
	}
	
//	@UiHandler("synonymScrollPanel")
//	void onClick(ClickEvent event){
//		ScrollPanel scroll = (ScrollPanel) event.getSource();
//		
//	}
	
	public boolean isInUse(){
		return instanceInUse;
	}
	
	public void setText(String dirtyRootWord) {
		lookupRootWord(dirtyRootWord);
	}

	public void updateDirtyRootWord(KeyUpEvent event){
		dirtyRootWord = rootWordTextField.getText();
	}
	
	/**
	 * Gets invoked when the default constructor is called
	 * and a string is provided in the ui.xml file.
	 */
	public String getText() {
		return synonymSubmitButton.getText();
	}
	
	private String textFormat(String dirtyWord){
		RegExp nonAlpha = RegExp.compile("\\W");
		RegExp alpha = RegExp.compile("\\w");
		SplitResult nonAlphaStrip = alpha.split(dirtyWord);
		prefix = nonAlphaStrip.get(0).toString();
		suffix = nonAlphaStrip.get(nonAlphaStrip.length() -1 ).toString();
		
		return dirtyWord.trim().toLowerCase().replaceAll(nonAlpha.getSource(), "");
	}

	private void getSynonyms(String cleanWord){
		queryMongo(cleanWord);
	}
	
		//XML Document (DOM)
	//composed of Node objects (basic unit of xml data)
	//specific instances of Nodes can be:
	//	Element - <aElement>some Text</aElement>
	//  Text - text between two Element Tags
	//	Comment
	//  Attr - attribute <aElement myAttribute="123" />
	private void processBigHugeThesaurusDataXML(String rootWord, String rawXML){
		if(rawXML.compareTo("404") == 0){
			return;
		}
		
		  try {
			    // parse the XML document into a DOM
				Document messageDom = XMLParser.parse(rawXML);
			    
			    Node docElement = messageDom.getDocumentElement();
			    if(docElement == null){
			    	return;
			    }
			    
			    NodeList words = docElement.getChildNodes();

			    ArrayList<String> wordsUIArray = new ArrayList<String>();
			    String[] wordsMongoData = new String[words.getLength()];
			    String wordsUIText = new String();
			    
			    for(int i = 0; i < words.getLength(); i++){
			    	Element wordElement = (Element) words.item(i);
			    	String wordText = wordElement.getChildNodes().item(0).getNodeValue();
			    	if(wordElement.getAttribute("r").compareTo("ant") != 0){
			    		wordsUIArray.add( wordText + " : " + wordElement.getAttribute("p"));
			    	}
			    	
			    	wordsMongoData[i] =  wordText + " : " + wordElement.getAttribute("p") + " : " + wordElement.getAttribute("r") + " : " + rootWord ;
			    }
			    
		    	updateListBox(wordsUIArray);
			    	
			    updateMongo(wordsMongoData);
			  

			  } catch (Exception e) {
			    Window.alert(e.getMessage() + "\n" + e.fillInStackTrace() + "\n" + e.getStackTrace() + "\n" + e.getCause());
			  }
	}
	

	private void queryMongo(final String cleanWord){
		String[] mongoQueryStr = {"rootWord",cleanWord};
				
		// Set up the callback object.
		AsyncCallback<String[]> callback = new AsyncCallback<String[]>() {
		    @Override
			public void onSuccess(String[] result) {
		    	if(result == null || result.length == 0){
		    		if(MONGO_DEBUG){System.out.println("Mongo Miss");}
		    		mongoMiss(cleanWord);
		    	}
		    	else{
		    		if(MONGO_DEBUG){System.out.println("Mongo Hit");}
		    		mongoHit(result);
		    	}
				
			}

			@Override
			public void onFailure(Throwable caught) {
//				mongoMiss(cleanWord);
				System.out.print(caught.getMessage());
				
			}
		};
		// Make the call to the stock price service.
		Globals.getGlobals().mongoCRUDSvc.queryMongo(mongoQueryStr, callback);
		
		
		
	}
	
	
	/**
	 * get synonyms & parts of speech from mongo
	 * @param result
	 */
	protected void mongoHit(String[] result) {
		
		ArrayList<String> wordsUIArray = new ArrayList<String>();
	    String wordsUIText = new String();
	    String word = "", partOfSpeech = "", relationship = "";
	    String[] attrParse;
	    
	    for(int i = 0; i < result.length; i++){
	    	ArrayList<String> resultArray = new ArrayList<String>(Arrays.asList(result[i].split("\" , \""))); //split attributes
	    	for(String attr : resultArray){
	    		if(attr.contains("word")){
	    			attrParse = attr.split("\" : \"");//split key - value
	    			word = attrParse[1];
	    		}
	    		if(attr.contains("partOfSpeech")){
	    			attrParse = attr.split("\" : \"");//split key - value
	    			partOfSpeech  = attrParse[1];
	    		}
	    		if(attr.contains("relationship")){
	    			attrParse = attr.split("\" : \"");//split key - value
	    			relationship = attrParse[1];
	    		}
	    		
	    		
	    	}
	    	if(relationship.compareTo("ant") != 0){
	    		wordsUIArray.add( word + " : " + partOfSpeech);
	    	}
	    	
	    }
	    updateListBox(wordsUIArray);
	
	}
	
	private void updateListBox(ArrayList<String> wordsUIArray){
	    if(wordsUIArray != null && !wordsUIArray.isEmpty() && synonymScrollPanel != null){
		    for(int i = 0; i < wordsUIArray.size(); i++){
		    	synonymScrollPanel.addItem(wordsUIArray.get(i));
		    }
	    }
	    synonymScrollPanel.setVisibleItemCount(10);
	    
	    scrollContainer.add(synonymScrollPanel);
	    
//	    if(wordsUIArray != null && !wordsUIArray.isEmpty() && synonymScrollPanel != null){
//	    for(int i = 0; i < wordsUIArray.size(); i++){
//	    	synonymScrollPanel.addItem(wordsUIArray.get(i));
//	    }
//	    }
//	    wordsUIText = formatUIScrollText(wordsUIArray.toArray(new String[wordsUIArray.size()]));
	    
//	    synonymScrollPanel.setWidget(new HTML(wordsUIText));
//	    synonymScrollPanel.setVisibleItemCount(10);
//	    scrollContainer.add(synonymScrollPanel);
//	    wordsUIText = formatUIScrollText(wordsUIArray.toArray(new String[wordsUIArray.size()]));
//	    synonymScrollPanel.setWidget(new HTML(wordsUIText));
	}
	
	/**
	 * get related words & parts of speech from BHThesaurus 
	 * @param cleanWord
	 */
	protected void mongoMiss(final String cleanWord) {
		// Initialize the service proxy.
		
				
		// Set up the callback object.
		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				// TODO: Do something with errors.
			}

			public void onSuccess(String result) {
				//						updateTable(result);
				processBigHugeThesaurusDataXML(cleanWord, result);
			}
		};
		// Make the call to the stock price service.
		Globals.getGlobals().bHThesaurusSvc.getSynonyms(cleanWord, callback);
	}

	private void updateMongo(String[] vocabData){ 
		// Set up the callback object.
		AsyncCallback<Void> callback = new AsyncCallback<Void>() {
		    @Override
			public void onSuccess(Void result) {
				
			}

			@Override
			public void onFailure(Throwable caught) {
				
			}
		};
		// Make the call to the stock price service.
		Globals.getGlobals().mongoCRUDSvc.update(vocabData, callback);
	}
	

	public void myShowDialog(final DialogBoxOpener opener) {
		this.opener = opener;
		synonymScrollPanel.clear();
		selectedWords.clear();
		selectedIndecies.clear();
		prefix = "";
		suffix = "";
		instanceInUse = true;
		center();
	}
			
}
