package co.globalwebadvisors.cosm.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


@RemoteServiceRelativePath("bigHugeThesaurus")
public interface BigHugeThesaurusService extends RemoteService{
		
	/**
	 * 
	 * @param cleanRootWord
	 * @return httpResponse containing a raw XML string
	 */
	String getSynonyms(String cleanRootWord);
}
